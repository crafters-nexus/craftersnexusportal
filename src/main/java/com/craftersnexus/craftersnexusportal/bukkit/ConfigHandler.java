package com.craftersnexus.craftersnexusportal.bukkit;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.inventory.ItemStack;
import org.bukkit.plugin.Plugin;

import java.io.File;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.logging.Logger;

public class ConfigHandler {
    private static File configFile;
    private static FileConfiguration config;
    private static final File df = getPlugin().getDataFolder();

    // Loads the config file
    private static void load() {
        configFile = new File(df, "config.yml");
        config = YamlConfiguration.loadConfiguration(configFile);
    }

    // Saves the config file
    private static void save() {
        try {
            config.save(configFile);
        } catch(Exception e) {
            Bukkit.getConsoleSender().sendMessage(ChatColor.RED + "Error saving config file: " + configFile.getName());
        }
    }

    // Get the plugin from the main class
    private static Plugin getPlugin() {
        return CraftersNexusPortal.getPlugin(CraftersNexusPortal.class);
    }

    public static Set<String> getWorlds() {
        load();
        return config.getConfigurationSection("worlds").getKeys(false);
    }

    public static Material getWorldMaterial(String world) {
        load();
        String materialName = config.getString("worlds." + world + ".material");
        if (materialName.equalsIgnoreCase("none")) materialName = "AIR";
        return Material.getMaterial(materialName);
    }

    public static String getWorldName(String world) {
        load();
        return config.getString("worlds." + world + ".name");
    }

    public static List<String> getWorldLores(String world) {
        load();
        return config.getStringList("worlds."+ world + ".lore");
    }

    public static String getServerName() {
        load();
        return config.getString("server-name");
    }

    public static Map<String, ItemStack> getUnlockItems(String world) {
        load();
        Logger logger = getPlugin().getLogger();
        Map<String, ItemStack> unlockItemstacks= new HashMap<>();
        List<Map<?, ?>> unlockItems = config.getMapList("worlds." + world + ".unlock-items");
        // Free unlock
        if (unlockItems.size() <= 0) {
            return null;
        }
        for(Map<?, ?> item: unlockItems) {
            if (!item.containsKey("material") || !item.containsKey("amount") || !item.containsKey("damage") || !item.containsKey("name")) {
                logger.warning("Unlock items missing in config for world: " + world);
                return null;
            }
            String matName = (String) item.get("material");
            if (matName.equalsIgnoreCase("none")) {
                return null;
            }
            Material material = Material.getMaterial(matName);
            if (material == null) {
                logger.warning("Null unlock item material in config for world: " + world);
                return null;
            }
            int unlockAmount = (Integer) item.get("amount");
            int damage = (Integer) item.get("damage");
            String name = (String) item.get("name");
            if (damage < 0) damage = 0;
            unlockItemstacks.put(name, new ItemStack(material, unlockAmount, (short) damage));
        }
        return unlockItemstacks;
    }

    public static short getWorldDamage(String world) {
        short damage = (short) config.getInt("worlds." + world + ".damage");
        if (damage < 0) damage = 0;
        return damage;
    }
}