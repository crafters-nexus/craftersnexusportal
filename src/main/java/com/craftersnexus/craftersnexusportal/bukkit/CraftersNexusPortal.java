package com.craftersnexus.craftersnexusportal.bukkit;

import com.craftersnexus.craftersnexusportal.bukkit.gui.PortalGUI;
import com.craftersnexus.craftersnexusportal.bukkit.listeners.InventoryClickListener;
import com.craftersnexus.craftersnexusportal.bukkit.listeners.PlayerInteractListener;
import org.bukkit.ChatColor;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.plugin.java.JavaPlugin;

import java.util.logging.Logger;


public class CraftersNexusPortal extends JavaPlugin {
    @Override
    public void onEnable() {
        Logger logger = this.getLogger();
        // Dump a configuration file
        saveDefaultConfig();
        logger.info("Registering event listeners");
        // Register some event listeners
        this.getServer().getPluginManager().registerEvents(new PlayerInteractListener(), this);
        this.getServer().getPluginManager().registerEvents(new InventoryClickListener(), this);


        logger.info("Registering commands");
        // Register debug command
        this.getCommand("cnpd").setExecutor((commandSender, command, label, args) -> {
            if (commandSender instanceof Player) {
                Player player = (Player) commandSender;
                ItemStack heldItem = player.getItemInHand();
                player.sendMessage(ChatColor.GREEN + "Item Material: " + heldItem.getType());
                player.sendMessage(ChatColor.GREEN + "Item unsafe damage: " + heldItem.getDurability());
                return true;
            } else {
                commandSender.sendMessage("You must be a player too use this command.");
                return false;
            }
        });

        // Register command to open ui
        this.getCommand("cnp").setExecutor((commandSender, command, label, args) -> {
            if (commandSender instanceof Player) {
                Player player = (Player) commandSender;
                // Make a new portal GUI and open it for the player
                PortalGUI gui = new PortalGUI(player);
                gui.openInventoryGUI();
                return true;
            } else {
                commandSender.sendMessage("You must be a player too use this command.");
                return false;
            }
        });
        logger.info("Finished initializing!");
    }
}
