package com.craftersnexus.craftersnexusportal.bukkit.listeners;

import com.craftersnexus.craftersnexusportal.bukkit.CraftersNexusPortal;
import com.craftersnexus.craftersnexusportal.bukkit.gui.PortalGUI;
import org.bukkit.block.Block;
import org.bukkit.block.BlockState;
import org.bukkit.block.Sign;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.player.PlayerInteractEvent;

import java.util.Arrays;
import java.util.logging.Logger;

public class PlayerInteractListener implements Listener {
    @EventHandler
    public void onInteract(PlayerInteractEvent event) {
        // Get a logger
        Logger logger = CraftersNexusPortal.getPlugin(CraftersNexusPortal.class).getLogger();
        // Get the block from the event
        Block block = event.getClickedBlock();
        Action action = event.getAction();
        if (block != null) {
            if (action == Action.RIGHT_CLICK_BLOCK) {
                BlockState state = block.getState();
                // If the block is a sign
                if (state instanceof Sign) {
                    // Cast the block to a sign, get the text on the sign and check for the word "portal"
                    try {
                        Sign sign = (Sign) state;
                        String[] signText = sign.getLines();
                        if (Arrays.asList(signText).contains("[portal]")) {
                            // Get the player from the event
                            Player player = event.getPlayer();
                            // Make a new portal GUI and open it for the player
                            PortalGUI gui = new PortalGUI(player);
                            gui.openInventoryGUI();
                        }
                    } catch (ClassCastException e) {
                        logger.warning("Tried to cast a block to a sign and was unable to. See Details: ");
                        e.printStackTrace();
                    }
                }
            }
        }
    }
}
