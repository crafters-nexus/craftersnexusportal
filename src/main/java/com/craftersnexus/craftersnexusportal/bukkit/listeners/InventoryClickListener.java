package com.craftersnexus.craftersnexusportal.bukkit.listeners;

import com.craftersnexus.craftersnexusportal.bukkit.ConfigHandler;
import com.craftersnexus.craftersnexusportal.bukkit.CraftersNexusPortal;
import com.craftersnexus.craftersnexusportal.bukkit.gui.PortalGUI;
import net.luckperms.api.LuckPerms;
import net.luckperms.api.model.data.DataMutateResult;
import net.luckperms.api.model.user.User;
import net.luckperms.api.node.Node;
import org.bukkit.*;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.ClickType;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.InventoryHolder;
import org.bukkit.inventory.ItemStack;
import org.bukkit.plugin.PluginManager;
import org.bukkit.plugin.RegisteredServiceProvider;

import java.util.Map;
import java.util.logging.Logger;

public class InventoryClickListener implements Listener {
    @EventHandler
    public void onInventoryClick(InventoryClickEvent event) {
        Inventory inv = event.getClickedInventory();
        if (inv == null) {
            return;
        }
        InventoryHolder inventoryHolder = inv.getHolder();
        if (inventoryHolder == null) {
            return;
        }
        if (inventoryHolder instanceof PortalGUI) {
            Logger logger = CraftersNexusPortal.getPlugin(CraftersNexusPortal.class).getLogger();
            PortalGUI gui = (PortalGUI) inventoryHolder;
            String serverName = ConfigHandler.getServerName();
            // Cancel the event
            event.setCancelled(true);
            // Check if the thing that clicked it was a player
            if(event.getWhoClicked() instanceof Player) {
                // Get the ItemStack, the player and the click type
                Player player = (Player) event.getWhoClicked();
                Location playerLoc = player.getLocation();
                ItemStack item = event.getCurrentItem();
                ClickType click = event.getClick();
                // Check if it's not a bad click type, not a null item or if the slot is not empty
                if(!badClick(click) && item != null && !item.getType().equals(Material.AIR)) {
                    player.playSound(playerLoc, Sound.CLICK, 1.0f, 1.0f);
                    String worldName = gui.getWorldNameForItem(item);
                    Server server = Bukkit.getServer();
                    PluginManager pm = server.getPluginManager();
                    String permString = "craftersnexusportal." + serverName + "." + worldName;
                    if (click.isShiftClick()) {
                        if (player.hasPermission(permString)) {
                            player.sendMessage(ChatColor.YELLOW + "You already have this world unlocked!");
                        } else {
                            if (pm.isPluginEnabled("LuckPerms")) {
                                // Get the provider for LP
                                RegisteredServiceProvider<LuckPerms> provider = Bukkit.getServicesManager().getRegistration(LuckPerms.class);
                                if (provider != null) {
                                    // Try to remove the items for the cost of unlocking
                                    boolean removedItems = doItemRemoval(player, worldName);
                                    if (removedItems) {
                                        // Get the luckperms API and add the permission
                                        LuckPerms api = provider.getProvider();
                                        User user = api.getPlayerAdapter(Player.class).getUser(player);
                                        DataMutateResult result = user.data().add(Node.builder(permString).build());
                                        // If it was successful, save the changes, send the player a message, reload the GUI and play a sound
                                        if (result.wasSuccessful()) {
                                            api.getUserManager().saveUser(user);
                                            player.sendMessage(ChatColor.GREEN + "Unlocked world!");
                                            gui.reloadGUI();
                                            player.playSound(playerLoc, Sound.NOTE_PLING, 1.0f, 1.0f);
                                        } else {
                                            // If it failed, tell the player and play a sound
                                            player.sendMessage(ChatColor.RED + "There was an error when giving you permission to go to a world!");
                                            player.playSound(playerLoc, Sound.NOTE_BASS, 1.0f, 1.0f);
                                        }
                                    } else {
                                        player.sendMessage(ChatColor.RED + "You do not have enough items to unlock this dimension.");
                                        player.playSound(playerLoc, Sound.NOTE_BASS, 1.0f, 1.0f);
                                    }
                                }
                            } else {
                                logger.warning("LuckPerms is not enabled but is required to use the portal plugin! Please install it!");
                                player.sendMessage(ChatColor.RED + "There was an error when giving you permission to go to a world!");
                                player.playSound(playerLoc, Sound.NOTE_BASS, 1.0f, 1.0f);
                            }
                        }
                    } else {
                        if (player.hasPermission(permString)) {
                            World world = gui.getWorldForItem(item);
                            if (world != null) {
                                player.sendMessage(ChatColor.GREEN + "Teleporting you to the selected dimension!");
                                // Play teleport sound from where they were so people around can hear it
                                player.playSound(playerLoc, Sound.ENDERMAN_TELEPORT, 3.0f, 1.0f);
                                Location loc = world.getSpawnLocation();
                                player.teleport(loc);
                                Location newLoc = player.getLocation();
                                // Play teleport sound at new location so the player hears it.
                                player.playSound(newLoc, Sound.ENDERMAN_TELEPORT, 3.0f, 1.0f);
                            } else {
                                player.sendMessage(ChatColor.RED + "There was an error when attempting to teleport you to the selected world... Please contact an administrator (and tell them the world may not be loaded)");
                                logger.info("Error finding world for item in GUI. The world may not be loaded! World: " + item.getItemMeta().getDisplayName());
                            }
                        } else {
                            player.sendMessage(ChatColor.RED + "You have not unlocked this world yet! Shift + click it to unlock!");
                        }
                    }
                }
            }
        }
    }

    private boolean badClick(ClickType click) {
        // Ignore drops and swaps using number keys
        return click.equals(ClickType.DROP) || click.equals(ClickType.CONTROL_DROP) || click.equals(ClickType.NUMBER_KEY);
    }

    private boolean doItemRemoval(Player player, String worldName) {
        // Get a reference to the player's inventory
        Inventory inv = player.getInventory();
        // Get the unlock items, if it returns null, return true so we can just proceed (unlock costs nothing)
        Map<String, ItemStack> unlockItems = ConfigHandler.getUnlockItems(worldName);
        if (unlockItems == null) {
            return true;
        }
        for (String itemName: unlockItems.keySet()) {
            ItemStack unlockItem = unlockItems.get(itemName);
            // Get the unlock material and required amount.
            Material type = unlockItem.getType();
            int requiredAmount = unlockItem.getAmount();
            // If the required amount is 0, make it 1 so we can check they actually have an item even though we don't want to remove it.
            if (requiredAmount < 0) requiredAmount = 1;
            // Find the amount of unlock items the user has
            int numItems = 0;
            for (int i = 0; i < inv.getSize(); i++) {
                ItemStack itemTemp = inv.getItem(i);
                if (itemTemp != null && itemTemp.getType() == type && itemTemp.getDurability() == unlockItem.getDurability()) {
                    numItems += itemTemp.getAmount();
                }
            }
            // User doesn't have enough of an unlock item. Cannot unlock
            if (numItems < requiredAmount) {
                return false;
            }
        }

        // There is enough items to remove, go through and remove all unlock items from the player's inventory
        for (String itemName: unlockItems.keySet()) {
            ItemStack unlockItem = unlockItems.get(itemName);
            // Get the unlock material and required amount.
            Material type = unlockItem.getType();
            int amount = unlockItem.getAmount();
            int size = inv.getSize();
            for (int slot = 0; slot < size; slot++) {
                ItemStack is = inv.getItem(slot);
                if (is == null) continue;
                if (type == is.getType() && is.getDurability() == unlockItem.getDurability()) {
                    int newAmount = is.getAmount() - amount;
                    if (newAmount > 0) {
                        is.setAmount(newAmount);
                        break;
                    } else {
                        inv.clear(slot);
                        amount = -newAmount;
                        if (amount == 0) break;
                    }
                }
            }
        }
        return true;
    }
}
