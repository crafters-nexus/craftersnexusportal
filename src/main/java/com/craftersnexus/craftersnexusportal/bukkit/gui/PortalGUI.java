package com.craftersnexus.craftersnexusportal.bukkit.gui;

import com.craftersnexus.craftersnexusportal.bukkit.ConfigHandler;
import com.craftersnexus.craftersnexusportal.bukkit.CraftersNexusPortal;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.World;
import org.bukkit.entity.Player;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.InventoryHolder;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import java.util.*;
import java.util.logging.Logger;

public class PortalGUI implements InventoryHolder {
    private final Inventory inventory;
    Set<String> worlds;
    HashMap<ItemStack, String> worldMap = new HashMap<>();
    Logger logger = CraftersNexusPortal.getPlugin(CraftersNexusPortal.class).getLogger();
    Player player;

    // Build the GUI
    public PortalGUI(Player player1) {
        worlds = ConfigHandler.getWorlds();
        player = player1;
        int guiSize = worlds.size();
        if (guiSize <= 9) guiSize = 9;
        else if (guiSize <= 18) guiSize = 18;
        else if (guiSize <= 27) guiSize = 27;
        String guiName = "CraftersNexusPortal";
        inventory = Bukkit.createInventory(this, guiSize, guiName);
    }

    public void buildInventory() {
        for (String world: worlds) {
            Material material = ConfigHandler.getWorldMaterial(world);
            short damage = ConfigHandler.getWorldDamage(world);
            String name = ConfigHandler.getWorldName(world);
            List<String> lore = ConfigHandler.getWorldLores(world);
            if (material == null) {
                logger.info("Null material in portals config for world: " + world);
            } else {
                ItemStack item = newGUIItem(material, damage, name, lore, world);
                worldMap.put(item, world);
                inventory.addItem(item);
            }
        }
    }

    public ItemStack newGUIItem(Material material, short damage, String name, List<String> lore, String worldName) {
        ItemStack item = new ItemStack(material, 1, damage);
        ItemMeta metaData = item.getItemMeta();
        if(!name.equals("default")) {
            metaData.setDisplayName(name);
        }
        if (!player.hasPermission("craftersnexusportal." + ConfigHandler.getServerName() + "." + worldName)) {
            Map<String, ItemStack> unlockItems = ConfigHandler.getUnlockItems(worldName);
            if (unlockItems != null) {
                lore.add(ChatColor.YELLOW + "Shift + Click to unlock!");
                lore.add(ChatColor.GREEN + "Unlock cost:");
                for(String itemName: unlockItems.keySet()) {
                    lore.add("    " + ChatColor.LIGHT_PURPLE + itemName + " x " + unlockItems.get(itemName).getAmount());
                }
            }
        }
        metaData.setLore(lore);
        item.setItemMeta(metaData);
        return item;
    }

    @Override
    public Inventory getInventory() {
        return inventory;
    }

    public void openInventoryGUI() {
        buildInventory();
        player.openInventory(inventory);
    }

    public World getWorldForItem(ItemStack item) {
        String worldName = worldMap.get(item);
        return Bukkit.getWorld(worldName);
    }

    public String getWorldNameForItem(ItemStack item) {
        return worldMap.get(item);
    }

    public void reloadGUI() {
        inventory.clear();
        buildInventory();
    }
}
