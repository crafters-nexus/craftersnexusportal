package com.craftersnexus.craftersnexusportal.sponge.gui;

import com.craftersnexus.craftersnexusportal.sponge.ConfigHandlerSponge;
import com.fireboyev.menuapi.objects.Button;
import com.fireboyev.menuapi.objects.Menu;
import net.luckperms.api.LuckPerms;
import net.luckperms.api.model.data.DataMutateResult;
import net.luckperms.api.model.user.User;
import net.luckperms.api.node.Node;
import ninja.leaping.configurate.commented.CommentedConfigurationNode;
import org.spongepowered.api.Sponge;
import org.spongepowered.api.data.DataQuery;
import org.spongepowered.api.data.key.Keys;
import org.spongepowered.api.entity.living.player.Player;
import org.spongepowered.api.event.item.inventory.ClickInventoryEvent;
import org.spongepowered.api.item.ItemType;
import org.spongepowered.api.item.inventory.Inventory;
import org.spongepowered.api.item.inventory.ItemStack;
import org.spongepowered.api.item.inventory.property.InventoryDimension;
import org.spongepowered.api.item.inventory.query.QueryOperationTypes;
import org.spongepowered.api.service.ProviderRegistration;
import org.spongepowered.api.text.Text;
import org.spongepowered.api.text.format.TextColors;
import org.spongepowered.api.world.World;

import java.util.ArrayList;
import java.util.Optional;
import java.util.Set;

public class PortalGUI {
    public static void create(Player plyr) {
        ConfigHandlerSponge configHandler = ConfigHandlerSponge.getInstance();
        CommentedConfigurationNode config = configHandler.getConfig();
        // Get configured worlds
        Set<Object> worlds = configHandler.getWorlds();

        // Calculate inventory dimensions
        InventoryDimension invDimension = new InventoryDimension(9, 1);
        if (worlds.size() <= 9) invDimension = new InventoryDimension(9, 1);
        else if (worlds.size() <= 18) invDimension = new InventoryDimension(9, 2);
        else if (worlds.size() <= 27) invDimension = new InventoryDimension(9, 3);

        Menu menu = new Menu("CraftersNexusPortal", invDimension);
        int index = 0;
        // For all worlds in the config
        for (Object worldKey: worlds) {
            // Get the info we need to make the GUI item and to set the unlock item
            CommentedConfigurationNode world = config.getNode("worlds", worldKey);
            String itemName = world.getNode("name").getString();
            String itemTypeString = world.getNode("item-type").getString();
            int unsafeDamage = world.getNode("unsafe-damage").getInt();
            String unlockItem = world.getNode("unlock-item").getString();
            String unlockItemName = world.getNode("unlock-item-name").getString();
            int unlockItemUnsafeDamage = world.getNode("unlock-item-unsafe-damage").getInt();
            String serverName = configHandler.getServerName();
            String worldPerm = "craftersnexusportal." + serverName + "." + worldKey;
            int unlockAmount = world.getNode("unlock-amount").getInt();

            // Skip invalid item types
            if (itemTypeString == null || !Sponge.getRegistry().getType(ItemType.class, itemTypeString).isPresent() || unlockItem == null) {
                continue;
            }
            // Create the ItemStack for the GUI and set it's display name
            ItemType itemType = Sponge.getRegistry().getType(ItemType.class, itemTypeString).get();
            ItemStack guiItem = ItemStack.builder().itemType(itemType).build();
            guiItem.offer(Keys.DISPLAY_NAME, Text.of(TextColors.LIGHT_PURPLE, itemName));

            // Set the unlock name to Free if it's supposed to be free
            if (unlockItem.equalsIgnoreCase("none")) {
                unlockItemName = "Free";
            }

            // Get the item lore and add it
            guiItem.offer(Keys.ITEM_LORE, getLore(plyr, worldPerm, unlockItemName, unlockAmount));

            // Make sure the unsafe damage is never below zero
            if (unsafeDamage < 0) unsafeDamage = 0;
            if (unlockItemUnsafeDamage < 0) unlockItemUnsafeDamage = 0;

            // Copy unlock item's unsafe damage value for use in lambda below.
            int finalUnlockItemUnsafeDamage = unlockItemUnsafeDamage;
            // Copy the unlock item name
            String finalUnlockItemName = unlockItemName;

            // Update the unsafe damage for the gui item
            guiItem = ItemStack.builder().fromContainer(guiItem.toContainer().set(DataQuery.of("UnsafeDamage"), unsafeDamage)).build();
            // Register the button in the menu and increment the index
            Button but = new Button(guiItem, index);
            menu.registerButton(but);
            index++;

            // Setup the button executor

            but.setExecutor((clickInventoryEvent, menu1, button, player) -> {
                boolean hasPerm = player.hasPermission(worldPerm);
                if (clickInventoryEvent instanceof ClickInventoryEvent.Primary) {
                    // SHIFT CLICK
                    if (clickInventoryEvent instanceof ClickInventoryEvent.Primary.Shift) {
                        if (hasPerm) {
                            // Player has the perm so just tell them.
                            player.sendMessage(Text.of(TextColors.YELLOW, "You already have this dimension unlocked!"));
                        } else {
                            // Player has no perms
                            // Get the LP provider
                            Optional<ProviderRegistration<LuckPerms>> provider = Sponge.getServiceManager().getRegistration(LuckPerms.class);
                            // If it's not present, there is something wrong
                            if (!provider.isPresent()) {
                                player.sendMessage(Text.of(TextColors.RED, "There was an error getting permission manager. Please contact an administrator"));
                                return;
                            }
                            // Get the API from the provider
                            LuckPerms api = provider.get().getProvider();
                            // Get the user
                            User user = api.getPlayerAdapter(Player.class).getUser(player);
                            // If the unlock item is not present
                            if (!Sponge.getRegistry().getType(ItemType.class, unlockItem).isPresent()) {
                                // Check if it's supposed to be none. If it is, the unlock is free, otherwise error
                                if(unlockItem.equalsIgnoreCase("none")) {
                                    addPerm(user, api, player, worldPerm, button, menu, finalUnlockItemName, unlockAmount);
                                } else {
                                    player.sendMessage(Text.of(TextColors.RED, "There was an error processing the unlock cost for this dimension! Please report this to an administrator."));
                                }
                            } else {
                                // Unlock item is present so we get it
                                ItemType unlockItemType = Sponge.getRegistry().getType(ItemType.class, unlockItem).get();
                                // Make sure the unlock amount we use for the ItemStack is never zero
                                int unlockAmountInt = unlockAmount;
                                if (unlockAmountInt == 0) unlockAmountInt++;
                                // Build the item stack and set the unsafe damage
                                ItemStack unlockItemStack = ItemStack.builder().itemType(unlockItemType).quantity(unlockAmountInt).build();
                                unlockItemStack = ItemStack.builder().fromContainer(unlockItemStack.toContainer().set(DataQuery.of("UnsafeDamage"), finalUnlockItemUnsafeDamage)).build();
                                // Try to  remove the unlock cost
                                boolean didRemove = doItemRemoval(player, unlockItemStack, unlockAmount);
                                // If we did, add the perm. If not, tell the user they need the items.
                                if (didRemove) {
                                    addPerm(user, api, player, worldPerm, button, menu, finalUnlockItemName, unlockAmount);
                                } else {
                                    player.sendMessage(Text.of(TextColors.YELLOW, "You lack the required items to unlock this dimension!"));
                                } 
                            }
                        }
                    } else {
                        // NORMAL LEFT CLICK
                        if (!hasPerm) {
                            player.sendMessage(Text.of(TextColors.RED, "You have not unlocked this dimension yet!"));
                        } else {
                            // Get the sponge world
                            Optional<World> spongeWorld = Sponge.getServer().getWorld(String.valueOf(worldKey));
                            if (spongeWorld.isPresent()) {
                                // Do teleport
                                player.sendMessage(Text.of(TextColors.GREEN, "Sending you to the selected dimension..."));
                                boolean teleported = player.setLocationSafely(spongeWorld.get().getSpawnLocation());
                                if (!teleported) {
                                    player.sendMessage(Text.of(TextColors.RED, "No safe location was found to teleport to. Please contact an administrator so they can add a safe location at the world spawn!"));
                                }
                            } else {
                                player.sendMessage(Text.of(TextColors.RED, "Unable to locate dimension with name: " + worldKey + ". Is it loaded?"));
                            }
                        }
                    }
                }
            });
        }
        menu.Open(plyr);
    }

    static ArrayList<Text> getLore(Player player, String worldPerm, String unlockItemName, int unlockAmount) {
        // Setup item lore
        ArrayList<Text> lore = new ArrayList<>();
        if (!player.hasPermission(worldPerm)) {
            lore.add(Text.of(TextColors.YELLOW, "Shift + Click to unlock!"));
            String ending = " x " + unlockAmount;
            if (unlockItemName.equalsIgnoreCase("Free")) {
                ending = "";
            }
            lore.add(Text.of(TextColors.GREEN, "Unlock cost: " + unlockItemName + ending));
        } else {
            lore.add(Text.of(TextColors.GREEN, "Click to teleport!"));
        }
        return lore;
    }

    static boolean doItemRemoval(Player player, ItemStack unlockItem, int quantity) {
        Inventory playerInv = player.getInventory();
        // Check if they have the unlock item in any quantity
        if (playerInv.query(QueryOperationTypes.ITEM_STACK_IGNORE_QUANTITY.of(unlockItem)).peek().isPresent()) {
            // If the unlock quantity is zero, return true
            if (quantity == 0) return true;
            // Check if the player has enough of the unlock item
            if (playerInv.contains(unlockItem)) {
                // Remove the items and return the status
                Optional<ItemStack> removed = playerInv.query(QueryOperationTypes.ITEM_STACK_IGNORE_QUANTITY.of(unlockItem)).poll(unlockItem.getQuantity());
                return removed.isPresent();
            } else {
                return false;
            }
        } else {
            return false;
        }
    }

    static void addPerm(User user, LuckPerms api, Player player, String permNode, Button button, Menu menu, String unlockItemName, int unlockAmount) {
        DataMutateResult result = user.data().add(Node.builder(permNode).build());
        if (result.wasSuccessful()) {
            api.getUserManager().saveUser(user);
            player.sendMessage(Text.of(TextColors.GREEN, "Unlocked world!"));
            ArrayList<Text> newLore = getLore(player, permNode, unlockItemName, unlockAmount);
            ItemStack buttonItem = button.getItemStack();
            buttonItem.remove(Keys.ITEM_LORE);
            buttonItem.offer(Keys.ITEM_LORE, newLore);
            button.setItemStack(buttonItem);
            menu.refresh(player);
        } else {
            player.sendMessage(Text.of(TextColors.RED, "There was an error when giving you permission to go to a world!"));
        }
    }
}
