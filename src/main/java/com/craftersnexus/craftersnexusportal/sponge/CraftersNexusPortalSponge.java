package com.craftersnexus.craftersnexusportal.sponge;


import com.craftersnexus.craftersnexusportal.sponge.listeners.InteractBlockListener;
import com.google.inject.Inject;
import ninja.leaping.configurate.commented.CommentedConfigurationNode;
import ninja.leaping.configurate.loader.ConfigurationLoader;
import org.slf4j.Logger;
import org.spongepowered.api.Sponge;
import org.spongepowered.api.command.CommandResult;
import org.spongepowered.api.command.spec.CommandSpec;
import org.spongepowered.api.config.DefaultConfig;
import org.spongepowered.api.data.DataQuery;
import org.spongepowered.api.data.type.HandTypes;
import org.spongepowered.api.entity.living.player.Player;
import org.spongepowered.api.event.Listener;
import org.spongepowered.api.event.game.state.GameInitializationEvent;
import org.spongepowered.api.event.game.state.GamePreInitializationEvent;
import org.spongepowered.api.item.inventory.ItemStack;
import org.spongepowered.api.plugin.Dependency;
import org.spongepowered.api.plugin.Plugin;
import org.spongepowered.api.text.Text;
import org.spongepowered.api.text.format.TextColors;

import java.io.File;
import java.util.Arrays;
import java.util.Optional;

@Plugin(
        id = "craftersnexusportal",
        version = "0.2",
        name = "CraftersNexusPortal",
        description = "Sponge version of the CraftersNexusPortal plugin",
        dependencies = {
                @Dependency(id = "menuapi"),
                @Dependency(id = "luckperms")
        },
        authors = {
                "Column01"
        }
)

public class CraftersNexusPortalSponge {
    @Inject
    @DefaultConfig(sharedRoot = false)
    private File configFile;

    @Inject
    @DefaultConfig(sharedRoot = false)
    ConfigurationLoader<CommentedConfigurationNode> configManager;

    @Inject
    private Logger logger;
    public Logger getLogger() {
        return logger;
    }

    @Listener
    public void preInit(GamePreInitializationEvent event) {
        logger.info("Loading CN portal plugin");
    }

    @Listener
    public void onInit(GameInitializationEvent event) {
        logger.info("Initializing Config");
        ConfigHandlerSponge.getInstance().setupConfig(configFile, configManager);
        logger.info("Worlds in config: " + ConfigHandlerSponge.getInstance().getWorlds());

        if (Sponge.getPluginManager().isLoaded("menuapi")) {
            logger.info("Setting up event listeners");
            Sponge.getEventManager().registerListeners(this, new InteractBlockListener());
            logger.info("Registering the debug command");
            // Build the simple debug command
            Sponge.getCommandManager().register(this, CommandSpec.builder()
                    .description(Text.of("Lists the held item's type"))
                    .permission("craftersnexusportal.debug")
                    .executor((src, args) -> {
                        if (src instanceof Player) {
                            Player player = (Player) src;
                            Optional<ItemStack> item = player.getItemInHand(HandTypes.MAIN_HAND);
                            item.ifPresent(itemStack -> player.sendMessage(Text.of(
                                    TextColors.GREEN,
                                    "Unsafe Damage: " + itemStack.toContainer().get(DataQuery.of("UnsafeDamage")),
                                    "\n",
                                    TextColors.GREEN,
                                    "ItemID: " + itemStack.getType().getId())));
                            return CommandResult.success();
                        } else {
                            src.sendMessage(Text.of("You must be a player to use this command"));
                            return CommandResult.empty();
                        }
                    })
                    .build(), Arrays.asList("cnpd", "craftersnexusportaldebug")
            );
            logger.info("Loaded successfully.");
        } else {
            logger.info("The MenuAPI plugin is required to use the sponge version of this plugin! Find it here: https://github.com/fireboyev/MenuAPI");
        }
    }
}
