package com.craftersnexus.craftersnexusportal.sponge;

import ninja.leaping.configurate.commented.CommentedConfigurationNode;
import ninja.leaping.configurate.loader.ConfigurationLoader;

import java.io.File;
import java.io.IOException;
import java.util.Set;

public class ConfigHandlerSponge {
    private static final ConfigHandlerSponge instance = new ConfigHandlerSponge();

    public static ConfigHandlerSponge getInstance() {
        return instance;
    }

    private ConfigurationLoader<CommentedConfigurationNode> configLoader;
    private CommentedConfigurationNode config;

    public void setupConfig(File configFile, ConfigurationLoader<CommentedConfigurationNode> configLoader) {
        this.configLoader = configLoader;

        if (!configFile.exists()) {
            try {
                boolean created = configFile.createNewFile();
                if (created) {
                    loadConfig();
                    config.getNode("server-name").setComment("The name for the server").setValue("server");
                    CommentedConfigurationNode worlds = config.getNode("worlds");
                    worlds.setComment("A list of worlds for the GUI");
                    setupWorlds(worlds);
                    saveConfig();
                } else {
                    System.out.println("Error creating config file!");
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        } else {
            loadConfig();
        }
    }

    public void setupWorlds(CommentedConfigurationNode worlds) {
        // Setting up the end
        CommentedConfigurationNode end = worlds.getNode("DIM1");
        end.getNode("name").setComment("The name in the GUI").setValue("The End");
        end.getNode("item-type").setComment("The item that appears in the GUI").setValue("minecraft:end_portal_frame");
        end.getNode("unsafe-damage").setComment("The variant of the item. For example, yellow wool is #0035/4 so the unsafe damage value would be 4.").setValue(0);
        end.getNode("unlock-item").setComment("The item to unlock this dimension").setValue("minecraft:ender_eye");
        end.getNode("unlock-item-name").setComment("The name of the unlock item for the GUI").setValue("Eye of Ender");
        end.getNode("unlock-item-unsafe-damage").setComment("The variant of the item to unlock this dimension.").setValue(0);
        end.getNode("unlock-amount").setComment("The amount of the item to unlock this dimension").setValue(12);

        // Setting up the nether
        CommentedConfigurationNode nether = worlds.getNode("DIM-1");
        nether.getNode("name").setValue("The Nether");
        nether.getNode("item-type").setValue("minecraft:obsidian");
        nether.getNode("unsafe-damage").setValue(0);
        nether.getNode("unlock-item").setValue("minecraft:flint_and_steel");
        nether.getNode("unlock-item-name").setValue("Flint and Steel");
        nether.getNode("unlock-item-unsafe-damage").setValue(0);
        nether.getNode("unlock-amount").setValue(0);
    }

    public Set<Object> getWorlds() {
        this.loadConfig();
        CommentedConfigurationNode worlds = config.getNode("worlds");
        return worlds.getChildrenMap().keySet();
    }

    public String getServerName() {
        this.loadConfig();
        return config.getNode("server-name").getString();
    }

    public void loadConfig() {
        try {
            config = configLoader.load();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void saveConfig() {
        try {
            configLoader.save(config);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public CommentedConfigurationNode getConfig() {
        return config;
    }
}
