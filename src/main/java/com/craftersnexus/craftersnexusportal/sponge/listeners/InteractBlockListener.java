package com.craftersnexus.craftersnexusportal.sponge.listeners;

import com.craftersnexus.craftersnexusportal.sponge.gui.PortalGUI;
import org.spongepowered.api.block.BlockSnapshot;
import org.spongepowered.api.block.BlockType;
import org.spongepowered.api.block.BlockTypes;
import org.spongepowered.api.block.tileentity.Sign;
import org.spongepowered.api.data.value.mutable.ListValue;
import org.spongepowered.api.entity.living.player.Player;
import org.spongepowered.api.event.Listener;
import org.spongepowered.api.event.block.InteractBlockEvent;
import org.spongepowered.api.event.filter.cause.First;
import org.spongepowered.api.text.Text;
import org.spongepowered.api.world.Location;
import org.spongepowered.api.world.World;

import java.util.Optional;


public class InteractBlockListener {
    @Listener
    public void onInteractBlockEvent(InteractBlockEvent event, @First Player player) throws NoSuchFieldException {
        // Grab a BlockSnapshot and get the the block type from it
        BlockSnapshot blockSnapshot = event.getTargetBlock();
        BlockType blockType = blockSnapshot.getState().getType();
        // Check if it's a valid click
        if (validClick(blockType, event)) {
            // Prevent other actions involving this event
            event.setCancelled(true);
            Optional<Location<World>> loc = blockSnapshot.getLocation();
            // Ignore it if no location is present
            if(!loc.isPresent()) {
                return;
            }
            // If the location contains a tile entity
            if (loc.get().getTileEntity().isPresent()) {
                // Cast the tile entity to a sign and check for the portal text
                Sign sign = (Sign) loc.get().getTileEntity().get();
                ListValue<Text> signText = sign.lines();
                for (Text line: signText) {
                    if (line.trim().toPlain().contains("[portal]")) {
                        // Create a GUI for the player
                        PortalGUI.create(player);
                    }
                }
            }
        }
    }

    boolean isSign(BlockType blockType) {
        return blockType == BlockTypes.STANDING_SIGN || blockType == BlockTypes.WALL_SIGN;
    }

    boolean validClick(BlockType blockType,InteractBlockEvent event) {
        return isSign(blockType) && event instanceof InteractBlockEvent.Secondary;
    }
}
