# CraftersNexusPortal

Simple portal plugin for Minecraft

## Bukkit Version

### Info

Signs that have the words `[portal]` on them will act as a portal sign. To use it, the user needs to right click the sign, and it will open the GUI. They will be teleported to the world spawn when they click one of the worlds.

**NOTE:** It's a wise idea to set the world spawn using essentials that is somewhere safe or else it can/will drop them into the void, suffocate them or drop them into a pool of lava. It will not validate teleport locations to see if they are safe or not, it just uses the world's spawn location.

### Adding new worlds

1. Get the world name (`DIM-1` for example) and add it as a new config section like the other worlds. 
2. Change it's `name` to whatever you want it to be in the GUI
3. Change it's `lore` section to whatever you want to be (maybe warnings and such?). 
4. Change the `material` to be whatever you want that is a valid [bukkit material](https://jd.bukkit.org/org/bukkit/Material.html). This is the item that shows up in the GUI.
5. To set the unlock items, copy a blob from the `worlds.<dimension>.unlock-items` section of another world and edit the values to whatever is returned by `/cnpd` when holding the unlock item.  
This is what each section for an unlock item means:
   - `name`
       - The name of the unlock item in the GUI
   - `material`
       - The bukkit material for the unlock item (use `/cnpd` to get it)
       - Set to `none` to make the dimension unlock free
   - `damage`
       - The sub-id of the unlock item (use `/cnpd` to get it)
   - `amount`
       - The amount you want it to cose
   - To have multiple items, copy the inner blob again and edit it to another item's info.
   - If you get lost, refer to the `unlock-items` section for the nether.

   


### Commands and Permissions

- `/cnpd` or `/craftersnexusportaldebug`
    - `craftersnexusportal.debug`
    - Shows the item type and unsafe damage of your held item
- `craftersnexusportal.<servername>.<worldname>`
	- Enables use of that world on that server. Server name is in the config file.

## Sponge Version

### Info

Signs that have the words `[portal]` on them will act as a portal sign. To use it, the user needs to right click the sign, and it will open the GUI. They will be teleported to the world spawn when they click one of the worlds.

**NOTE:** It's a wise idea to set the world spawn using nucleus that is somewhere safe, it will not teleport a player if the area is not safe.

### Adding new worlds

1. Get the world name (`DIM-1` for example) and add it as a new config section like the other worlds. 
2. Change it's `name` to whatever you want it to be in the GUI
3. Set the GUI Item:
    - Set `item-type` to the item ID (`minecraft:diamond_ore`)
    - Set `unsafe-damage` to the variant number. 
        - For example, Yellow wool is `#0035/4` so this value would be `4` for a yellow wool to unlock it. Set it to zero for anything with no variants.
4. Set the unlock item: 
    - Set `unlock-item` to the item ID (`minecraft:diamond`)
    - Set `unlock-amount` to a whole number. 
    - Set `unlock-item-name` to a human readable name for the item.
    - Set `unlock-item-unsafe-damage` to the variant number. (see above on normal GUI items for details.)

#### Info on unlock items

If you want it to just check for an item, set the `unlock-amount` to `0`. If you want it to be free, set the `unlock-item` to `None`.

### Commands and Permissions

- `/cnpd` or `/craftersnexusportaldebug`
	- `craftersnexusportal.debug`
	- Shows the item type and unsafe damage of your held item
- `craftersnexusportal.<servername>.<worldname>`
	- Enables use of that world on that server. Server name is in the config file.

## To develop

Download LuckPerms API called `api-5.1.jar` from [here](https://repo1.maven.org/maven2/net/luckperms/api/5.1/) and place it in the `libs` folder.

For IntellijI run: `gradle idea` and then business as usual:

- `gradle clean`
- `gradle build`
